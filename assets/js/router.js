const route = (event) => {
  event = event || window.event;
  event.preventDefault();
  window.history.pushState({}, "", event.target.href);
  handleLocation();
};

const routes = {
  404: "/pages/404.html",
  "/": "/pages/index.html",
  "/index.html": "/pages/index.html",
  "/planety": "/pages/planety.html",
  "/zaloga": "/pages/zaloga.html",
  "/technologie": "/pages/technologie.html",
};

const handleLocation = async () => {
  const path = window.location.pathname;
  const route = routes[path] || routes[404];
  const html = await fetch(route).then((data) => data.text());
  const links = document.querySelectorAll('#primary-navigation li a');
  const mainContent = document.querySelector('main');

  mainContent.classList.remove('see');

  document.getElementById("main-page").innerHTML = html;

  if (path === "/pages/index.html" || path === "/" || path === "/index.html") {
    document.body.className = '';
    document.body.classList.add("home");
    setTimeout(() => {
      mainContent.classList.add('see');
    }, 100);
  } else if (path === "/pages/planety.html" || path === "/planety") {
    document.body.className = '';
    document.body.classList.add("destination");
    setTimeout(() => {
      mainContent.classList.add('see');
    }, 100);
  } else {
    document.body.className = '';
  }

  links.forEach(link => {
    const href = link.getAttribute('href');
    if (href === path) {
        link.parentElement.classList.add('active');
    } else {
        link.parentElement.classList.remove('active');
    }
  });

};

window.onpopstate = handleLocation;
window.route = route;

document.addEventListener("DOMContentLoaded", function() {
  handleLocation();
});