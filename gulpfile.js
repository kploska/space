const { src, dest, watch, series } = require('gulp')
const sass = require('gulp-sass')(require('sass'))
// const postcss = require('postcss')
const prefix = require('gulp-autoprefixer')

function buildStyles() {
    return src('./scss/*.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(prefix())
        .pipe(dest('assets/css', { sourcemaps: '.' }))
}

function watcher() {
    watch(['./scss/*.scss'], buildStyles)
}

exports.default = series(buildStyles, watcher)